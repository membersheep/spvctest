//
//  AppDelegate.h
//  SPVCTest
//
//  Created by Alessandro Maroso on 13/02/14.
//  Copyright (c) 2014 waresme. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
