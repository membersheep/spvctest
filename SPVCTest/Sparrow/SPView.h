//
//  SPView.h
//  Sparrow
//
//  Created by Robert Carone on 2/5/14.
//  Copyright 2013 Gamua. All rights reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the Simplified BSD License.
//

#import <UIKit/UIKit.h>
#import <Sparrow/SPMacros.h>

@class SPContext;
@class SPRectangle;
@protocol SPViewDelegate;

/** ------------------------------------------------------------------------------------------------

 SPView manages and OpenGL context and framebuffer and should be used in conjunction with 
 SPViewController in most cases.

 _You don't have to use this class directly in most cases._

------------------------------------------------------------------------------------------------- */

@interface SPView : UIView

/// -------------
/// @name Methods
/// -------------

/// Renders a frame, will call "drawRect:" if no delegate has been set.
- (void)render;

/// Binds the context and drawable.
- (void)bindDrawable;

/// Deletes the backing framebuffer, ususally called when an application is backgrounded.
- (void)deleteDrawable;

/// ----------------
/// @name Properties
/// ----------------

/// If a delegate is provided, it is called instead of calling a drawRect: method whenever the
/// view’s contents need to be drawn.
@property (nonatomic, assign) id<SPViewDelegate> delegate;

/// The OpenGL context used when drawing the view’s contents. Never change the context from inside
/// your drawing method.
@property (nonatomic, strong) SPContext *context;

/// Indicates if the view is currently rendering a frame.
@property (nonatomic, readonly) BOOL rendering;

/// The width, in pixels, of the underlying framebuffer object.
@property (nonatomic, readonly) int drawableWidth;

/// The height, in pixels, of the underlying framebuffer object.
@property (nonatomic, readonly) int drawableHeight;

@end


/** ------------------------------------------------------------------------------------------------
   
 A delegate that allows you to provide a drawing and reshaping of OpenGL context without
 subclassing SPView. 

------------------------------------------------------------------------------------------------- */

@protocol SPViewDelegate <NSObject>
@required

/// Delegate method used to render OpenGL content.
- (void)spView:(SPView *)view drawWithRectangle:(SPRectangle *)rectangle;

@optional

/// Delegate method used to reshape OpenGL content.
- (void)spView:(SPView *)view reshapeWithRectangle:(SPRectangle *)rectangle;

@end
