//
//  SPView.m
//  Sparrow
//
//  Created by Robert Carone on 2/8/14.
//  Copyright 2013 Gamua. All rights reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the Simplified BSD License.
//

#import <Sparrow/SPContext_Internal.h>
#import <Sparrow/SPDisplayLink.h>
#import <Sparrow/SPOpenGL.h>
#import <Sparrow/SPRectangle.h>
#import <Sparrow/SPView.h>

#pragma mark - SPView

@interface SPView ()
@property (nonatomic, readonly) CAEAGLLayer *layer;
@end

@implementation SPView
{
    id<SPViewDelegate> _delegate;
    SPContext *_context;
    dispatch_queue_t _renderQueue;
    float _prevBackingScaleFactor;
    int _drawableWidth;
    int _drawableHeight;
    uint _colorRenderBuffer;
    uint _depthStencilRenderBuffer;
    uint _frameBuffer;
    BOOL _hasRenderedOnce;
    BOOL _rendering;
    BOOL _paused;
}

@dynamic layer;

#pragma mark Initialization

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
        [self setup];
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
        [self setup];

    return self;
}

- (void)setup
{
    _prevBackingScaleFactor = 0;
    _renderQueue = dispatch_queue_create("Sparrow-RenderQueue", NULL);

    self.userInteractionEnabled = YES;
    self.multipleTouchEnabled = YES;
    self.contentScaleFactor = 0;
}

- (void)dealloc
{
    [self deleteDrawable];
    [(id)_renderQueue release];
    [_context release];

    [super dealloc];
}

#pragma mark Methods

- (void)render
{
    __block id weakSelf = self;
    [self executeBlockInRenderQueue:^{
        [weakSelf renderAndPresentRenderbuffer:YES];
    }];
}

- (void)bindDrawable
{
    __block id weakSelf = self;
    [self executeBlockInRenderQueue:^{
        [weakSelf bindFramebuffer];
    }];
}

- (void)deleteDrawable
{
    __block id weakSelf = self;
    [self executeBlockInRenderQueue:^{
        [weakSelf deleteFramebuffer];
        _prevBackingScaleFactor = 0;
    }];
}

- (void)executeBlockInRenderQueue:(dispatch_block_t)block
{
    if (!_rendering)
        dispatch_sync(_renderQueue, block);
    else
        block();
}

#pragma mark View

+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

- (BOOL)isOpaque
{
    return YES;
}

- (void)displayLayer:(CALayer *)layer
{
    [self render];
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    [self executeBlockInRenderQueue:^{
        [self remakeFrameBufferWithScaleFactor:_prevBackingScaleFactor];
    }];

    if (_delegate && [_delegate respondsToSelector:@selector(spView:reshapeWithRectangle:)])
        [_delegate spView:self reshapeWithRectangle:[SPRectangle rectangleWithCGRect:self.bounds]];
}

#pragma mark OpenGL

- (BOOL)bindOpenGLContext
{
    if (self.context) return [SPContext setCurrentContext:self.context];
    else              return NO;
}

- (SPContext *)context
{
    return [[_context retain] autorelease];
}

- (void)setContext:(SPContext *)context
{
    if (context != _context)
    {
        SP_RELEASE_AND_RETAIN(_context, context);
        _prevBackingScaleFactor = 0;

        if ([self bindOpenGLContext])
            [self prepareOpenGLState];
    }
}

- (void)setupContext
{
    SPContext *globalContext = [SPContext sharedContext];
    if (globalContext)
        self.context = globalContext;
    else
    {
        self.context = [SPContext context];
        [SPContext setSharedContext:self.context];
    }

    if ([self bindOpenGLContext])
    {
        self.opaque = YES;
        self.clearsContextBeforeDrawing = NO;
    }
    else
        NSLog(@"Failed to create OpenGL context.");
}

- (void)prepareOpenGLState
{
    glDisable(GL_CULL_FACE);
    glDisable(GL_DITHER);
    glDisable(GL_STENCIL_TEST);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glDepthFunc(GL_ALWAYS);
    glBlendColor(0.0, 0.0, 0.0, 1.0);
    glLineWidth(1.0);
}

- (void)renderAndPresentRenderbuffer:(BOOL)presentRenderbuffer
{
    _rendering = YES;

    if (!_context)
        [self setupContext];

    if ([self bindOpenGLContext])
    {
        BOOL renderedOnce = _hasRenderedOnce;
        glPushGroupMarkerEXT(0, "Rendering");

        [self bindFramebuffer];

        if (_delegate && [_delegate respondsToSelector:@selector(spView:drawWithRectangle:)])
            [_delegate spView:self drawWithRectangle:[SPRectangle rectangleWithCGRect:self.bounds]];
        else
            [self drawRect:self.bounds];

        static const GLenum attachments[] = { GL_STENCIL_ATTACHMENT, GL_DEPTH_ATTACHMENT };
        glDiscardFramebufferEXT(GL_FRAMEBUFFER, 2, attachments);

        if (presentRenderbuffer)
        {
            glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
            [_context.nativeContext presentRenderbuffer:GL_RENDERBUFFER];
        }

        glPopGroupMarkerEXT();
        _hasRenderedOnce = YES;

        if (!renderedOnce && _hasRenderedOnce)
            glFinish();
    }

    _rendering = NO;
}

- (void)bindFramebuffer
{
    if ([self bindOpenGLContext])
    {
        float backingScaleFactor;
        if (self.window.screen) backingScaleFactor = self.window.screen.scale;
        else                    backingScaleFactor = UIScreen.mainScreen.scale;

        if (backingScaleFactor != _prevBackingScaleFactor)
        {
            [self remakeFrameBufferWithScaleFactor:backingScaleFactor];
            _prevBackingScaleFactor = backingScaleFactor;
        }

        glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
        glViewport(0, 0, _drawableWidth, _drawableHeight);
    }
}

- (void)deleteFramebuffer
{
    if ([self bindOpenGLContext])
    {
        glPushGroupMarkerEXT(0, "Delete Framebuffer");

        if (_frameBuffer)
        {
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            glDeleteFramebuffers(1, &_frameBuffer);
            _frameBuffer = 0;
        }

        if (_colorRenderBuffer || _depthStencilRenderBuffer)
        {
            glBindRenderbuffer(GL_RENDERBUFFER, 0);

            if (_colorRenderBuffer)
            {
                glDeleteRenderbuffers(1, &_colorRenderBuffer);
                _colorRenderBuffer = 0;
            }

            if (_depthStencilRenderBuffer)
            {
                glDeleteRenderbuffers(1, &_depthStencilRenderBuffer);
                _depthStencilRenderBuffer = 0;
            }
        }

        glPopGroupMarkerEXT();
    }
}

- (void)remakeFrameBufferWithScaleFactor:(float)scaleFactor
{
    if ([self bindOpenGLContext])
    {
        self.layer.opaque = YES;
        self.layer.contentsScale = scaleFactor;

        glPushGroupMarkerEXT(0, "Create Framebuffer");

        if (_frameBuffer)
            glDeleteBuffers(1, &_frameBuffer);

        if (_colorRenderBuffer)
            glDeleteBuffers(1, &_colorRenderBuffer);

        if (_depthStencilRenderBuffer)
            glDeleteBuffers(1, &_depthStencilRenderBuffer);

        glGenBuffers(1, &_frameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);

        // color render buffer

        glGenBuffers(1, &_colorRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);

        [_context.nativeContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:self.layer];
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_drawableWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_drawableHeight);

        // depth and stencil render buffer

        glGenRenderbuffers(1, &_depthStencilRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, _depthStencilRenderBuffer);

        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8_OES, _drawableWidth, _drawableHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthStencilRenderBuffer);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, _depthStencilRenderBuffer);

        glPopGroupMarkerEXT();

        GLenum error = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (error != GL_FRAMEBUFFER_COMPLETE)
        {
            NSLog(@"Sparrow failed to make complete framebuffer object %s", sglGetErrorString(error));
            [self deleteFramebuffer];
        }
        
        _hasRenderedOnce = NO;
    }
}

@end
