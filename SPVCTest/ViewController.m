//
//  ViewController.m
//  SPVCTest
//
//  Created by Alessandro Maroso on 13/02/14.
//  Copyright (c) 2014 waresme. All rights reserved.
//

#import "ViewController.h"
#import "SPViewController.h"
#import "Game.h"

@interface ViewController ()
{
    SPViewController* _viewController;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"VIEWDIDLOAD");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPressed
{
    NSLog(@"BUTTON PRESSED");
    // Load Sparrow View Controller
    _viewController = [[SPViewController alloc] init];
    _viewController.targetFramesPerSecond = 60;
    [_viewController startWithRoot:[Game class] supportHighResolutions:YES doubleOnPad:YES];
    [self.navigationController pushViewController:_viewController animated:YES];
}

@end
