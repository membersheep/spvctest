//
//  ViewController.h
//  SPVCTest
//
//  Created by Alessandro Maroso on 13/02/14.
//  Copyright (c) 2014 waresme. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIButton* button;

- (IBAction)buttonPressed;

@end
